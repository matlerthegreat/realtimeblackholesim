#!/bin/sh

rm -rf Vulkan/examples/computeraytracing
rm -rf Vulkan/data/shaders/glsl/computeraytracing

stow -t Vulkan src

cd Vulkan
cmake . -DUSE_WAYLAND_WSI=ON
./download_assets.py
