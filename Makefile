SHADERS_DIR = src/data/shaders/glsl/computeraytracing
SRC_DIR = src/examples/computeraytracing
OUT_DIR = Vulkan/bin

all: $(SHADERS_DIR)/raytracing.comp.spv $(SHADERS_DIR)/texture.frag.spv $(SHADERS_DIR)/texture.vert.spv $(OUT_DIR)/computeraytracing

$(SHADERS_DIR)/raytracing.comp.spv: $(SHADERS_DIR)/raytracing.comp
	glslangValidator -V $(SHADERS_DIR)/raytracing.comp -o $(SHADERS_DIR)/raytracing.comp.spv

$(SHADERS_DIR)/texture.frag.spv: $(SHADERS_DIR)/texture.frag
	glslangValidator -V texture.frag -o $(SHADERS_DIR)/texture.frag.spv

$(SHADERS_DIR)/texture.vert.spv: $(SHADERS_DIR)/texture.vert
	glslangValidator -V $(SHADERS_DIR)/texture.vert -o $(SHADERS_DIR)/texture.vert.spv

$(OUT_DIR)/computeraytracing: $(SRC_DIR)/computeraytracing.cpp
	make -C Vulkan computeraytracing
