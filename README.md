# Real-time Black Hole Simulator

This  simulation aims at providing realtime Schwarzschild black hole simulation using the Runge-Kutta numerical method for solving the four dimentional geodesic equation in Vulkan's Compute Shaders.

# Results

![Schwarzschild black hole](media/showoff.png "Schwarzschild black hole")
![Schwarzschild black hole](media/interstellar.png "Schwarzschild black hole")